import { Action } from '@ngrx/store';
import { ActionWithPayload } from '../interfaces/actionWithPayload';

export enum BikeTypesActionsTypes {
  Retrieve = '[BIKE_TYPES] Retrieve',
  RetrieveSuccess = '[BIKE_TYPES] Retrieve Success',
  RetrieveError = '[BIKE_TYPES] Retrieve Error',
}

export class RetrieveBikeTypesAction implements Action {
  readonly type = BikeTypesActionsTypes.Retrieve;
}

export class RetrieveBikeTypesSuccessAction implements ActionWithPayload {
  readonly type = BikeTypesActionsTypes.RetrieveSuccess;
  constructor(public payload: any) {
  }
}

export class RetrieveBikeTypesErrorAction implements ActionWithPayload {
  readonly type = BikeTypesActionsTypes.RetrieveError;
  constructor(public payload: any) {
  }
}

export type BikeTypesActionsUnion =
  RetrieveBikeTypesAction |
  RetrieveBikeTypesSuccessAction |
  RetrieveBikeTypesErrorAction;
