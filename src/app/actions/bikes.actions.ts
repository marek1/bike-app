import { ActionWithPayload } from '../interfaces/actionWithPayload';
import { CompanyFilterAndQuery } from '../interfaces/companyFilterAndQuery';

export enum BikesActionsTypes {
  Retrieve = '[BIKES] Retrieve',
  RetrieveSuccess = '[BIKES] Retrieve Success',
  RetrieveError = '[BIKES] Retrieve Error',
}

export class RetrieveBikesAction implements ActionWithPayload {
  readonly type = BikesActionsTypes.Retrieve;
  constructor(public payload: CompanyFilterAndQuery) {
    console.log('payload : ', payload);
  }
}

export class RetrieveBikesSuccessAction implements ActionWithPayload {
  readonly type = BikesActionsTypes.RetrieveSuccess;
  constructor(public payload: any) {
  }
}

export class RetrieveBikesErrorAction implements ActionWithPayload {
  readonly type = BikesActionsTypes.RetrieveError;
  constructor(public payload: any) {
  }
}

export type BikesActionsUnion =
  RetrieveBikesAction |
  RetrieveBikesSuccessAction |
  RetrieveBikesErrorAction;
