import { ActionWithPayload } from '../interfaces/actionWithPayload';
import { CompanyFilterAndQuery } from '../interfaces/companyFilterAndQuery';
import { Action } from '@ngrx/store';

export enum UserActionsTypes {
  Retrieve = '[USER] Retrieve',
  RetrieveSuccess = '[USER] Retrieve Success',
  RetrieveError = '[USER] Retrieve Error',
}

export class RetrieveUserAction implements Action {
  readonly type = UserActionsTypes.Retrieve;
  constructor() {
  }
}

export class RetrieveUserSuccessAction implements ActionWithPayload {
  readonly type = UserActionsTypes.RetrieveSuccess;
  constructor(public payload: any) {
  }
}

export class RetrieveUserErrorAction implements ActionWithPayload {
  readonly type = UserActionsTypes.RetrieveError;
  constructor(public payload: any) {
  }
}

export type UserActionsUnion =
  RetrieveUserAction |
  RetrieveUserSuccessAction |
  RetrieveUserErrorAction;
