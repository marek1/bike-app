import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { getBikesData, getUserHistory, getUserId, hasBikesError, isBikesLoading } from './reducer';
import { Observable, Subscription } from 'rxjs';
import { RetrieveBikesAction } from './actions/bikes.actions';
import { BikesList } from './interfaces/bikesList';
import { ActivatedRoute, Router } from '@angular/router';
import { ElementRef } from '@angular/core';
import { Bike } from './interfaces/bike';
import { map } from 'rxjs/internal/operators';
import { CompanyFilterAndQuery } from './interfaces/companyFilterAndQuery';
import { SortTypes } from './enums/sortTypes';
import { ClickService } from './services/click.service';
import { BikeTypes } from './data/bikeTypes';
import { FrameTypes } from './data/frameTypes';
import { GearingTypes } from './data/gearingTypes';
import { GearingManufacturer } from './data/gearingManufacturer';
import { BreakingTypes } from './data/breakingTypes';
import { RetrieveUserAction } from './actions/user.actions';
import { FolkTypes } from './data/folkTypes';
import { AntriebTypes } from './data/antriebTypes';
import { Questions } from './data/questions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit, OnDestroy {
  // type definitions
  userId: string;
  userHistory$: Observable<Bike[]>;
  bikes$: Observable<BikesList>;
  bikesFiltered$: Observable<Bike[]>;
  isBikesLoading$: Observable<any>;
  hasBikesError$: Observable<any>;
  lastRequest: CompanyFilterAndQuery;
  suggestions: string[];
  searchQueryForm: FormGroup;
  routeSub: Subscription;
  companyFilter: string;
  resultsDomElement: any;
  filterOn: boolean;
  sortAttribute: string;
  selectedBikeTypes: any[];

  selectedManufacturers: any[];
  selectedFrameTypes: any[];
  selectedFrameSizes: any[];
  selectedSchaltungen: any[];
  selectedSchaltgruppen: any[];
  selectedBremsen: any[];
  selectedFolks: any[];
  selectedAntriebe: any[];

  manufacturers: any[];
  frames: any[];
  framesizes: any[];
  schaltungen: any[];
  schaltgruppen: any[];
  bremsen: any[];
  folks: any[];
  antriebe: any[];

  visibleBikeTypes: string[];
  visibleFrameTypes: string[];
  visibleGearingManufacturer: string[];

  activeSlide: number;

  // straight assignments
  sortAttributes = SortTypes;
  bikeTypes = BikeTypes;
  frameTypes = FrameTypes;
  breakingTypes = BreakingTypes;
  gearingTypes = GearingTypes;
  gearingManufacturers = GearingManufacturer;
  folkTypes = FolkTypes;
  antriebTypes = AntriebTypes;

  questions = Questions;
  startOfRequest: string = 'Ich suche ein Fahrrad';
  constructedSearchQuery: string = '';

  currentYear = new Date().getFullYear();

  constructor(public router: Router,
              private store$: Store<any>,
              private myElement: ElementRef,
              private activatedRoute: ActivatedRoute,
              private clickService: ClickService) {
    this.lastRequest = {
      companyFilter: '',
      query: ''
    };
    this.suggestions = [
      'Ich suche ein Rad für die Arbeit',
      'Ich suche ein Rennrad mit Shimano Schaltung'
    ];
    this.searchQueryForm = new FormGroup({
      searchQuery: new FormControl('')
    });
    this.companyFilter = '';
    this.filterOn = false;
    this.visibleBikeTypes = [];
    this.visibleFrameTypes = [];
    this.visibleGearingManufacturer = [];
    // selected
    this.selectedBikeTypes = [];
    this.selectedManufacturers = [];
    this.selectedFrameTypes = [];
    this.selectedFrameSizes = [];
    this.selectedSchaltungen = [];
    this.selectedSchaltgruppen = [];
    this.selectedBremsen = [];
    this.selectedFolks = [];
    this.selectedAntriebe = [];
    //
    this.manufacturers = [];
    this.frames = [];
    this.framesizes = [];
    this.schaltungen = [];
    this.schaltgruppen = [];
    this.bremsen = [];
    this.folks = [];
    this.antriebe = [];
    //
    this.activeSlide = 0;
  }

  ngOnInit() {
    this.store$.dispatch(new RetrieveUserAction());
    this.resultsDomElement = this.myElement.nativeElement.querySelectorAll('.results');
    const user$ = this.store$.pipe(select(getUserId));
    user$.subscribe((data) => {
      this.userId = data;
    });
    this.userHistory$ = this.store$.pipe(select(getUserHistory));
    this.bikes$ = this.store$.pipe(select(getBikesData));
    this.bikes$.subscribe((data: BikesList) => {
      if (data.bikes.length > 0) {
        window.setTimeout(() => {
          this.resultsDomElement[0].scrollIntoView({ behavior: 'smooth', block: 'start' });
        }, 100);

        // reset all
        this.manufacturers = [];
        this.frames = [];
        this.framesizes = [];
        this.schaltungen = [];
        this.schaltgruppen = [];
        this.bremsen = [];
        this.folks = [];
        this.antriebe = [];

        // fill filter
        data.bikes.map((bike) => {
          if (this.manufacturers.indexOf(bike.manufacturer) < 0) {
            this.manufacturers.push(bike.manufacturer);
          }
          if (this.frames.indexOf(bike.frame) < 0) {
            this.frames.push(bike.frame);
          }
          const framesizes = bike.framesize.split(',');
          framesizes.map((framesize) => {
            if (this.framesizes.indexOf(framesize) < 0) {
                this.framesizes.push(framesize);
            }
          });
          if (this.schaltungen.indexOf(bike.gearing_type) < 0) {
            this.schaltungen.push(bike.gearing_type);
          }
          if (this.schaltgruppen.indexOf(bike.schaltgruppe) < 0) {
            this.schaltgruppen.push(bike.schaltgruppe);
          }
          if (this.bremsen.indexOf(bike.break_type) < 0) {
            this.bremsen.push(bike.break_type);
          }
          if (this.folks.indexOf(bike.gabel) < 0) {
            if (bike.gabel.length > 2) {
              this.folks.push(bike.gabel);
            }
          }
          if (this.antriebe.indexOf(bike.antrieb) < 0) {
            if (bike.antrieb.length > 2) {
              this.antriebe.push(bike.antrieb);
            }
          }
        });
      }
    });
    this.setFilteredBikes();
    this.isBikesLoading$ = this.store$.pipe(select(isBikesLoading));
    this.hasBikesError$ = this.store$.pipe(select(hasBikesError));
    this.routeSub = this.activatedRoute.queryParams.subscribe((params) => {
      if (params['c'] !== undefined && params['c']) {
        this.companyFilter = params['c'];
      }
    });
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }

  toggleActive(opt: any, questionIndex, optionIndex) {
    const x = this.questions[questionIndex].options[optionIndex].active;
    let currentSearchQuery = this.constructedSearchQuery;
    this.questions[questionIndex].options.map((op) => {
      op.active = false;
      if (op.value && currentSearchQuery.indexOf(' und ' + op.value) > -1) {
        currentSearchQuery = currentSearchQuery.replace(' und ' + op.value, '');
      } else if (op.value && currentSearchQuery.indexOf(op.value) > -1) {
        currentSearchQuery = currentSearchQuery.replace(op.value, '');
      }
    });
    currentSearchQuery = currentSearchQuery.trim();
    this.questions[questionIndex].options[optionIndex].active = true;
    if (currentSearchQuery.length === 0) {
      currentSearchQuery = this.startOfRequest;
    }
    if (currentSearchQuery.indexOf(opt.value) < 0) {
      let fillWord = ' ';
      if (currentSearchQuery.length > this.startOfRequest.length) {
        fillWord = ' und ';
      }
      currentSearchQuery += fillWord + opt.value;
    }
    this.constructedSearchQuery = currentSearchQuery;
  }

  resetSearchQuery() {
    this.constructedSearchQuery = '';
  }

  useSearchQuery(query: string) {
    this.searchQueryForm.controls['searchQuery'].setValue(query);
    this.submitForm();
    this.router.navigateByUrl('/');
  }

  isActiveSlide(i: number) {
    return i === this.activeSlide;
  }

  slide(i, length) {
    if (i < 0 && this.activeSlide === 0) {
      this.activeSlide = length - 1;
    } else if (i > 0 && this.activeSlide === length - 1) {
      this.activeSlide = 0;
    } else {
      this.activeSlide += i;
    }
  }

  toggleFilter() {
    if (this.filterOn) {
      this.filterOn = false;
    } else {
      this.filterOn = true;
    }
  }

  setFilteredBikes() {
    // filter bikes...
    this.bikesFiltered$ = this.bikes$
      .pipe(
        map(data => data.bikes),
        map(bikes => bikes.filter((bike) => {
          const framesizes = bike.framesize.split(',');
          let isSelectedFrameSize = false;
          framesizes.map((framesize) => {
            if (this.isSelectedFrameSize(framesize)) {
              isSelectedFrameSize = true;
            }
          });
          return (this.selectedBikeTypes.length < 1 || this.isSelectedBikeType(bike.bike_type_id)) &&
            (this.selectedManufacturers.length < 1 || this.isSelectedManufacturer(bike.manufacturer)) &&
            (this.selectedFrameTypes.length < 1 || this.isSelectedFrameType(bike.frame)) &&
            (this.selectedFrameSizes.length < 1 || isSelectedFrameSize) &&
            (this.selectedSchaltungen.length < 1 || this.isSelectedSchaltung(bike.gearing_type)) &&
            (this.selectedSchaltgruppen.length < 1 || this.isSelectedSchaltgruppe(bike.schaltgruppe)) &&
            (this.selectedFolks.length < 1 || this.isSelectedFolk(bike.gabel)) &&
            (this.selectedAntriebe.length < 1 || this.isSelectedAntrieb(bike.antrieb))
        }))
      );
  }

  setSearchQuery(queryString: string) {
    this.searchQueryForm.controls['searchQuery'].setValue(queryString);
  }

  submitForm() {
    // submitting
    const query = this.searchQueryForm.controls['searchQuery'].value;
    if ((!this.companyFilter || (this.lastRequest.companyFilter !== this.companyFilter)) || this.lastRequest.query !== query) {
      // call action
      this.store$.dispatch(new RetrieveBikesAction({
        companyFilter: this.companyFilter,
        query: query
      }));
      // and update last request
      this.lastRequest.companyFilter = this.companyFilter;
      this.lastRequest.query = query;
      // reset filter
      this.selectedBikeTypes = [];
      this.selectedManufacturers = [];
      this.selectedFrameTypes = [];
      this.selectedFrameSizes = [];
      this.selectedSchaltungen = [];
      this.selectedSchaltgruppen = [];
      this.selectedBremsen = [];
      this.selectedFolks = [];
      this.selectedAntriebe = [];
    }
  }

  toggleBikeTypeSelection(id: number) {

    // submit click to server
    this.clickService.postClickEvent(this.userId, id, 0);

    if (this.isSelectedBikeType(id)) {
      this.selectedBikeTypes.splice(this.selectedBikeTypes.indexOf(id), 1);
    } else {
      this.selectedBikeTypes.push(id);
    }
    this.setFilteredBikes();
  }

  isSelectedBikeType(id: number) {
    return this.selectedBikeTypes.indexOf(id) > -1 ? true : false;
  }

  toggleManufacturerSelection(id: string) {
    if (this.isSelectedManufacturer(id)) {
      this.selectedManufacturers.splice(this.selectedManufacturers.indexOf(id), 1);
    } else {
      this.selectedManufacturers.push(id);
    }
    this.setFilteredBikes();
  }

  isSelectedManufacturer(id: string) {
    return this.selectedManufacturers.indexOf(id) > -1 ? true : false;
  }

  toggleFrameSelection(id: string) {
    if (this.isSelectedFrameType(id)) {
      this.selectedFrameTypes.splice(this.selectedFrameTypes.indexOf(id), 1);
    } else {
      this.selectedFrameTypes.push(id);
    }
    this.setFilteredBikes();
  }

  isSelectedFrameType(id: string) {
    return this.selectedFrameTypes.indexOf(id) > -1 ? true : false;
  }

  toggleFrameSizeSelection(id: string) {
    if (this.isSelectedFrameSize(id)) {
      this.selectedFrameSizes.splice(this.selectedFrameSizes.indexOf(id), 1);
    } else {
      this.selectedFrameSizes.push(id);
    }
    this.setFilteredBikes();
  }

  isSelectedFrameSize(id: string) {
    return this.selectedFrameSizes.indexOf(id) > -1 ? true : false;
  }

  toggleSchaltungenSelection(id: string) {
    if (this.isSelectedSchaltung(id)) {
      this.selectedSchaltungen.splice(this.selectedSchaltungen.indexOf(id), 1);
    } else {
      this.selectedSchaltungen.push(id);
    }
    this.setFilteredBikes();
  }

  isSelectedSchaltung(id: string) {
    return this.selectedSchaltungen.indexOf(id) > -1 ? true : false;
  }

  toggleSchaltgruppeSelection(id: string) {
    if (this.isSelectedSchaltgruppe(id)) {
      this.selectedSchaltgruppen.splice(this.selectedSchaltgruppen.indexOf(id), 1);
    } else {
      this.selectedSchaltgruppen.push(id);
    }
    this.setFilteredBikes();
  }

  isSelectedSchaltgruppe(id: string) {
    return this.selectedSchaltgruppen.indexOf(id) > -1 ? true : false;
  }

  toggleBremsenSelection(id: string) {
    if (this.isSelectedBremse(id)) {
      this.selectedBremsen.splice(this.selectedBremsen.indexOf(id), 1);
    } else {
      this.selectedBremsen.push(id);
    }
    this.setFilteredBikes();
  }

  isSelectedBremse(id: string) {
    return this.selectedBremsen.indexOf(id) > -1 ? true : false;
  }

  toggleFolkSelection(id: string) {
    if (this.isSelectedFolk(id)) {
      this.selectedFolks.splice(this.selectedFolks.indexOf(id), 1);
    } else {
      this.selectedFolks.push(id);
    }
    this.setFilteredBikes();
  }

  isSelectedFolk(id: string) {
    return this.selectedFolks.indexOf(id) > -1 ? true : false;
  }

  toggleAntriebeSelection(id: string) {
    if (this.isSelectedAntrieb(id)) {
      this.selectedAntriebe.splice(this.selectedAntriebe.indexOf(id), 1);
    } else {
      this.selectedAntriebe.push(id);
    }
    this.setFilteredBikes();
  }

  isSelectedAntrieb(id: string) {
    return this.selectedAntriebe.indexOf(id) > -1 ? true : false;
  }

  forwardToBike(bike: Bike) {

    // submit click to server
    this.clickService.postClickEvent(this.userId, bike.bike_type_id, bike.bike_id);

    // open bike in new tab
    (window as any).open(bike.url, '_blank');

  }

  sortBy(ev: any) {
    this.sortAttribute = ev.target.value;
    if (this.sortAttribute === this.sortAttributes.NAME_ASC || this.sortAttribute === this.sortAttributes.NAME_DESC) {
      this.sortByName(this.sortAttribute);
    } else if (this.sortAttribute === this.sortAttributes.PRICE_ASC || this.sortAttribute === this.sortAttributes.PRICE_DESC) {
      this.sortByPrice(this.sortAttribute);
    }
  }

  sortByPrice(sortBy: string) {
    if (sortBy === this.sortAttributes.PRICE_ASC) {
      this.bikesFiltered$ = this.bikesFiltered$.pipe(
        map(data => data.sort(this.sortPriceAsc))
      );
    } else if (sortBy === this.sortAttributes.PRICE_DESC) {
      this.bikesFiltered$ = this.bikesFiltered$.pipe(
        map(data => data.sort(this.sortPriceDesc))
      );
    }
  }

  sortByName(sortBy: string) {
    if (sortBy === this.sortAttributes.NAME_ASC) {
      this.bikesFiltered$ = this.bikesFiltered$.pipe(
        map(data => data.sort(this.sortNameAsc))
      );
    } else if (sortBy === this.sortAttributes.NAME_DESC) {
      this.bikesFiltered$ = this.bikesFiltered$.pipe(
        map(data => data.sort(this.sortNameDesc))
      );
    }
  }

  includes(bikeArray: Bike[], key: string, val: string) {
    let returnValue = false;
    bikeArray.map((bike) => {
      if (bike[key] !== undefined && bike[key] === val) {
        returnValue = true;
      }
    });
    return returnValue;
  }

  toggleBikeType(id: string) {
    if (this.isVisibleBikeType(id)) {
      this.visibleBikeTypes.splice(this.visibleBikeTypes.indexOf(id), 1);
    } else {
      this.visibleBikeTypes.push(id);
    }
  }

  isVisibleBikeType(id: string) {
    return this.visibleBikeTypes.indexOf(id) > -1 ? true : false;
  }

  toggleFrameType(id: string) {
    if (this.isVisibleFrameType(id)) {
      this.visibleFrameTypes.splice(this.visibleFrameTypes.indexOf(id), 1);
    } else {
      this.visibleFrameTypes.push(id);
    }
  }

  isVisibleFrameType(id: string) {
    return this.visibleFrameTypes.indexOf(id) > -1 ? true : false;
  }

  toggleGearingManufacturer(id: string) {
    if (this.isVisibleGearingManufacturer(id)) {
      this.visibleGearingManufacturer.splice(this.visibleGearingManufacturer.indexOf(id), 1);
    } else {
      this.visibleGearingManufacturer.push(id);
    }
  }

  isVisibleGearingManufacturer(id: string) {
    return this.visibleGearingManufacturer.indexOf(id) > -1 ? true : false;
  }

  splitString(str: string) {
    if (str.indexOf(',') < 0) {
      return str;
    }
    return str.split(',').join(', ');
  }

  private sortPriceAsc(a: Bike, b: Bike) {
    if (isNaN(parseFloat(a.price_from_in_euro)) || isNaN(parseFloat(b.price_from_in_euro))) {
      return 0;
    }
    return parseFloat(a.price_from_in_euro) - parseFloat(b.price_from_in_euro);
  }

  private sortPriceDesc(a: Bike, b: Bike) {
    if (isNaN(parseFloat(a.price_from_in_euro)) || isNaN(parseFloat(b.price_from_in_euro))) {
      return 0;
    }
    return parseFloat(b.price_from_in_euro) - parseFloat(a.price_from_in_euro);
  }

  private sortNameAsc(a: Bike, b: Bike) {
    if (a.name < b.name) { return -1; }
    if (a.name > b.name) { return 1; }
    return 0;
  }

  private sortNameDesc(a: Bike, b: Bike) {
    if (a.name > b.name) { return -1; }
    if (a.name < b.name) { return 1; }
    return 0;
  }
}
