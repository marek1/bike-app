import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { BikesEffects } from './effects/bikes.effects';
import { HttpClientModule } from '@angular/common/http';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { BikeTypesEffects } from './effects/bikeTypes.effects';
import { RouterModule } from '@angular/router';
import { ClickService } from './services/click.service';
import { reducerProvider, reducerToken } from './reducer';
import { UserEffects } from './effects/user.effects';
import { LazyLoadImagesModule } from 'ngx-lazy-load-images';

// export const apiUrl = 'http://0.0.0.0:5000';
export const apiUrl = 'http://vengasport.de/bike-api';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {
        path: 'fahrradtypen',
        component: AppComponent
      },
      {
        path: 'rahmen',
        component: AppComponent
      },
      {
        path: 'rahmengroessen',
        component: AppComponent
      },
      {
        path: 'schaltungen',
        component: AppComponent
      },
      {
        path: 'schaltgruppen',
        component: AppComponent
      },
      {
        path: 'bremsen',
        component: AppComponent
      },
      {
        path: 'gabel',
        component: AppComponent
      },
      {
        path: 'antrieb',
        component: AppComponent
      },
      {
        path: 'impressum',
        component: AppComponent
      },
      {
        path: 'datenschutz',
        component: AppComponent
      },
      {
        path: 'helfer',
        component: AppComponent
      },
      {
        path: '**',
        component: AppComponent
      },
    ]),
    StoreModule.forRoot(reducerToken),
    StoreDevtoolsModule.instrument({
      maxAge: 5
    }),
    EffectsModule.forRoot([UserEffects, BikesEffects, BikeTypesEffects]),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    LazyLoadImagesModule
  ],
  providers: [
    reducerProvider,
    ClickService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
