export const AntriebTypes = [
  {
    'description': 'Der Mittelmotor befindet sich im Bereich des Tretlagers.',
    'image_source': 'Fahrrad XXL',
    'image_url': 'https://www.fahrrad-xxl.de/media/k14669/k14673/thumbs/71675_169133.jpg',
    'name': 'Mittelmotor',
    'advantages': ['gute Schwerpunktverlagerung', 'geringes Gewicht'],
    'disadvantages': ['hoher Preis', 'hohe Belastung auf Kette und Ritzel'],
  }, {
    'description': 'Der Hinterradmotor bzw. Nabenmotor befindet sich im Bereich des Hinterrades, genauer gesagt ist er in / an der Nabe verbaut.',
    'image_source': 'Fahrrad XXL',
    'image_url': 'https://www.fahrrad-xxl.de/media/k14669/k14673/thumbs/71677_169137.jpg',
    'name': 'Hinterradmotor / Nabenmotor ',
    'advantages': ['gute Traktion', 'gute Rekuperation (Energierückgewinnung)', 'kann nachträglich eingebaut werden', 'bietet größere Übersetzungsbandbreite'],
    'disadvantages': ['Schwerpunkt auf Hinterrad'],
  },
];
