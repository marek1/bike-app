export const BikeTypes = [{
  'description': 'Ein Rennrad dient vor allem der sportlichen Betätigung (Sportrad / Sportfahrrad). Spitzensportler sowie Amateure trainieren auf solchen einem solchem Rad ihre Ausdauer, in flachen Gebieten als auch in den Bergen. Rennradfahren ist ein beliebter Sport bei Jung und Alt. Die Sitzposition ist ebenso sportlich nach vorn neigend. Ein leichter Rahmen und die dünnen Reifen (18mm - 25mm) sorgen für eine schnelle, reibungsarme Fortbewegung mit diesem Fahrrad. Während ältere Modelle Rahmen aus Eisen (Eisenrahmen) bestückt sein können, haben moderne Rennräder meist Rahmen aus Carbon (Carbonrahmen) oder Aluminium (Aluminiumrahmen), womit sie sehr leicht sind. Spezifisch für Rennräder sind der Rennlenker / Rennradlenker, der ein Umfassen erlaubt. Bei Rennrädern kommt eine Gangschaltung mit sechzehn, achtzehn, einundzwanzig oder mehr Gängen zum Einsatz. Typische Hersteller sind Shimano (mit Dura-Ace, Ultegra, 105, Tiagra), SRAM oder Campagnolo. Das Triathlonrad ist verwandt, das Fitnessbike eine Alternative',
  'image_source': 'Wiel Rent',
  'image_url': 'https://www.wiel-rent.nl/images/huur-racefiets-wielrenfiets.jpg',
  'name': 'Rennrad',
  'id': 'rennrad'
}, {
  'description': 'Ein Urbanbike, Citybike oder Stadtfahrrad / Stadtrad dient vor allem der Fortbewegung in der Stadt. Es wird zur Fahrt an die Arbeit / ins Büro genutzt. Auch gemütliche Fahrten ins Umland sind möglich. Meist handelt es sich hier um ein leichtes Fahrrad mit einem Rahmen aus Aluminium. Ein solches Rad hat meist einen geraden Lenker und eine Gangschaltung mit drei bis acht Gängen. Die Reifen sind meist dünn (18mm - 28mm). Oftmals werden hier auch eine Nabenschaltung zum Einsatz, die wartungsärmer ist. Die Sitzposition ist nach vorn geneigt. Das Singlespeed und das Fixie ist verwandt, das Hollandrad eine Alternative. Des Weiteren gibt es auch das E-Urbanbike, E-Citybike oder E-Stadtfahrrad',
  'image_source': '2rad Weigand',
  'image_url': 'https://bikes.rim.de/IDUpload-1472531-435-265-1-0.jpg',
  'name': 'Urbanbike / Cityrad',
  'id': 'urbanbike'
}, {
  'description': 'Ein E-Urbanbike, E-Citybike oder E-Stadtfahrrad ist ein Stadtrad mit Elektroantrieb (E-Bike / Pedelec / E-Fahrrad / E-Rad). Es wird zur Fahrt an die Arbeit / ins Büro genutzt. Auch gemütliche Fahrten ins Umland sind möglich. Meist handelt es sich hier um ein leichtes Fahrrad mit einem Rahmen aus Aluminium. Ein solches Rad hat meist einen geraden Lenker und eine Gangschaltung mit drei bis acht Gängen. Die Reifen sind meist dünn (18mm - 28mm). Oftmals werden hier auch eine Nabenschaltung zum Einsatz, die wartungsärmer ist. Die Sitzposition ist nach vorn geneigt.',
  'image_source': '2rad Weigand',
  'image_url': 'https://bikes.rim.de/bikID-104147-435-265-1-0.jpg',
  'name': 'E-Urbanbike / E-Cityrad',
  'id': 'e-urbanbike'
}, {
  'description': 'Ein Hollandrad dient dem Fortkommen in der Stadt, vor allem für Fahrten auf flacher Strecke. Ein Hollandrad ist meist robust und der Rahmen aus Stahl. Es wiegt daher mehr als die meisten anderen Fahrräder. Die Reifen haben eine mittlere Dicke und sind profiliert. Das Fahrrad wird oft unter der Kategorie Urbanbike eingeordnet. Das Rad unterscheidet sich allerdings durch die Sitzposition, die aufrecht ist',
  'image_source': '2rad Weigand',
  'image_url': 'https://www.fahrrad.de/sf-media/iscommerce-shop/category_brand_header/FDE/listHeader/categoryHeader_37164_2.jpeg',
  'name': 'Hollandrad',
  'id': 'hollandrad'
}, {
  'description': 'Ein Fixie oder fixed-gear ist ein beliebtes Rad bei Fahrradkurieren und Fahrradenthusiasten, um durch die Stadt zu kommen. Es kommt ursprünglich aus dem Bahnradfahren bzw. dem Kirei (Japan). Das Fahrrad zeichnet sich dadurch aus, daß es ohne Gangschaltung auskommt. Die Reifen sind meist dünn (18-28mm). Im Gegensatz zum Singlespeed verfügt über eine Starrnabe, so daß jeder Tritt in die Pedale eins zu eins übersetzt wird. Das Urbanbike eine Alternative zum Fixie',
  'image_source': 'TheFixedGearShop.de',
  'image_url': 'https://www.thefixedgearshop.de/pub/media/catalog/product/cache/c687aa7517cf01e65c009f6943c2b1e9/f/i/fixie_sc-1_white_front.jpg',
  'name': 'Fixie',
  'id': 'fixie'
}, {
  'description': 'Ein Singlespeed ist ein beliebtes Rad bei Fahrradkurieren und Fahrradenthusiasten, um durch die Stadt zu kommen. Das Fahrrad zeichnet sich dadurch aus, daß es ohne Gangschaltung auskommt. Im Gegensatz zum Fixie verfügt es über eine Freilaufnarbe, so daß es ohne Treten in die Pedalen rollt. Die Reifen sind meist dünn (18mm - 28mm). Das Singlespeed wird oft unter der Kategorie Urbanbike eingeordnet. Das Fixie ist verwandt',
  'image_source': 'Fahrrad.de',
  'image_url': 'https://images.internetstores.de/products//694659/02/cb407c/FIXIE_Inc__Blackheath_Black[640x480][640x480].jpg?forceSize=true&forceAspectRatio=true&useTrim=true?forceSize=true&forceAspectRatio=true&useTrim=true',
  'name': 'Singlespeed',
  'id': 'singlespeed'
}, {
  'description': 'Ein Trekkingrad oder Tekkingbike ist ein robustes Fahrrad, welches sich für längere Ausflüge, z.B. an einem Wochenende, eignet. Bei einem solchen Rad kommt meist ein Aluminiumrahmen zum Einsatz, welches eine gute Stabilität für Fahrten auf asphaltierten Wegen und über unbefestigte Wege / Feldwege / Forstwege / Schotter bietet. Die Reifen haben eine mittlere Dicke und sind profiliert. Meist bietet ein Gepäckträger die Möglichkeit Gepäck mitzunehmen oder Fahrradtaschen daran zu befestigen. Das Fahrrad verfügt über eine Gangschaltung mit sechzehn, achtzehn, einundzwanzig oder mehr Gängen. Die Montage von Schutzblechen und Gepäckträger sind üblich. Natürlich gibt es auch ein E-Trekkingrad. Das Fitnessbike und das Allroundbike / Allrounder sind Alternativen',
  'image_source': 'Fahrrad.de',
  'image_url': 'https://images.internetstores.de/products//885999/02/8e7e6a/Ortler_Saragossa_Herren_schwarz_matt[640x480].jpg?forceSize=true&forceAspectRatio=true&useTrim=true',
  'name': 'Trekkingrad',
  'id': 'trekkingrad'
}, {
  'description': 'Ein E-Trekkingrad oder E-Trekkingbike ist ein Trekkingrad mit Elektroantrieb (E-Bike / Pedelec / E-Fahrrad / E-Rad). Das E-Bike ist ein robustes Fahrrad, welches sich für längere Ausflüge, z.B. an einem Wochenende, eignet. Bei einem solchen Rad kommt meist ein Aluminiumrahmen zum Einsatz, welches eine gute Stabilität für Fahrten auf asphaltierten Wegen und über unbefestigte Wege / Feldwege / Forstwege / Schotter bietet. Die Reifen haben eine mittlere Dicke und sind profiliert. Meist bietet ein Gepäckträger die Möglichkeit Gepäck mitzunehmen oder Fahrradtaschen daran zu befestigen. Das Fahrrad verfügt über einen geraden Lenker und eine Gangschaltung mit sechzehn, achtzehn, einundzwanzig oder mehr Gängen. Die Montage von Schutzblechen und Gepäckträger sind üblich. Natürlich gibt es auch ein nicht elektrisches Trekkingrad. Das Fitnessbike und das Allroundbike / Allrounder sind Alternativen',
  'image_source': 'Fahrrad.de',
  'image_url': 'https://images.internetstores.de/products//901002/02/624ae7/Ortler_Bozen_Performance_Powertube_Herren_black_matt[1920x1920].jpg?forceSize=false&forceAspectRatio=true&useTrim=true',
  'name': 'E-Trekkingrad',
  'id': 'e-trekkingrad'
}, {
  'description': 'Ein Mountainbike (oder MTB) ist ein Fahrrad, welches sich für den Einsatz auf nicht asphaltierten, unwegsamen Gelände (unbefestigte Wege / Feldwege / Forstwege / Schotter / Wald / Offroad) eignet. Durch die breiten, profilierten Reifen (50 - 60mm) kann das Rad über viele Unwegsamkeiten hinweg vorwärts bewegt werden. Besonders beliebt ist die Fahrt auf dünnen Pfaden, die durch den Wald oder unebenes Gelände (Singletrack) oder durch sogenannte Bikeparks führen. Auch Abfahrten von Bergen oder Hügeln (Downhill) sind Einsatzgebiete von Mountainbikes. Natürlich eignet es sich auch für eine Fahrt ins Büro oder für eine Ausfahrt am Wochenende. Allerdings ist zu beachten, daß die Fahrräder schwer sind und mit ihren breiten (dicken) Reifen für viel Reibung sorgen. Mountainbikes haben einen geraden Lenker und verfügen über eine Gangschaltung mit sechzehn, achtzehn, einundzwanzig, oder mehr Gängen. Des Weiteren verfügen viele Mountainbikes über eine Scheibenbremse. Es gibt auch sogenannte E-MTB oder E-Mountainbike, die mit einem Elektroantrieb / einer Batterie versehen sind. Es ist verwandt mit dem Fatbike. Gravelbike, Cyclocross / Crosser und Allroundbike / Allrounder sind Alternativen zum MTB',
  'image_source': 'Fahrrad.de',
  'image_url': 'https://www.fahrrad.de/sf-media/iscommerce-shop/category_brand_header/FDE/listHeader/categoryHeader_36898_1_20170808160153.jpeg',
  'name': 'Mountainbike / MTB',
  'id': 'mountainbike'
}, {
  'description': 'Ein E-Mountainbike (E-MTB) ist ein Mountainbike mit Elektroantrieb (E-Bike / Pedelec / E-Fahrrad / E-Rad). Das E-Bike ist ein Fahrrad, welches sich für den Einsatz auf nicht asphaltierten, unwegsamen Gelände (unbefestigte Wege / Feldwege / Forstwege / Schotter / Wald / Offroad) eignet. Durch die breiten, profilierten Reifen (50 - 60mm) kann das Rad über viele Unwegsamkeiten hinweg vorwärts bewegt werden. Besonders beliebt ist die Fahrt auf dünnen Pfaden, die durch den Wald oder unebenes Gelände (Singletrack) oder durch sogenannte Bikeparks führen. Auch Abfahrten von Bergen oder Hügeln (Downhill) sind Einsatzgebiete von Mountainbikes. Natürlich eignet es sich auch für eine Fahrt ins Büro oder für eine Ausfahrt am Wochenende. Allerdings ist zu beachten, daß die Fahrräder schwer sind und mit ihren breiten (dicken) Reifen für viel Reibung sorgen. Mountainbikes haben einen geraden Lenker und verfügen über eine Gangschaltung mit sechzehn, achtzehn, einundzwanzig, oder mehr Gängen. Des Weiteren verfügen viele Mountainbikes über eine Scheibenbremse. Natürlich gibt es auch nicht elektrische Mountainbikes',
  'image_source': 'Fahrrad.de',
  'image_url': 'https://images.internetstores.de/products//857554/02/cf9fde/Cube_Acid_Hybrid_ONE_400_Allroad_Grey_n_White[640x480].jpg?forceSize=true&forceAspectRatio=true&useTrim=true',
  'name': 'E-Mountainbike / E-MTB',
  'id': 'e-mountainbike'
}, {
  'description': 'Ein Triathlonrad ist ein spezielles Rad, welches hauptsächlich bei einem Triathlon zum Einsatz kommt. Meist handelt es sich um ein sehr leichtes Fahrrad mit einem Rahmen aus Carbon (Carbonrahmen) oder auch Aluminium (Aluminiumrahmen). Es zeichnet sich durch einen speziellen Lenker aus, an dem eine Ablage für die Ellbogen angebracht ist, so daß eine stark nach vorne geneigte Haltung minimalen Windwiderstand bzw. maximale Aerodynamik bietet. Damit können hohe Geschwindigkeiten erreicht werden. Die dünnen Reifen (18mm - 23mm) erzeugen minimale Reibungsverluste und erlauben ein schnelles Fortkommen. Das Rennrad ist eine Alternative',
  'image_source': 'Fahrrad.de',
  'image_url': 'https://images.internetstores.de/products//856747/02/9fc845/ORBEA_Ordu_M20_black_red[640x480].jpg?forceSize=true&forceAspectRatio=true&useTrim=true',
  'name': 'Triathlonrad',
  'id': 'triathlonrad'
}, {
  'description': 'Ein Cyclocross oder Crosser ist ein Fahrrad, welches auch abseits von Asphalt, asphaltierte Wegen gefahren werden kann (unbefestigte Wege / Feldwege / Forstwege / Schotter / Wald / Offroad). Es ist ein Mischung aus Rennrad und Mountainbike und wird vor allem zur sportlichen Betätigung eingesetzt. Besonders beliebt ist die Fahrt auf dünnen Pfaden, die durch den Wald oder unebenes Gelände (Singletrack). Der Rahmen ist meist aus Carbon (Carbonrahmen) oder Aluminium (Aluminiumrahmen). Der Lenker entspricht dem des Rennrads (Rennradlenker). Die Reifen sind dicker als beim Rennrad und verfügen über ausreichend Profil, so daß auch unebenes Geländer befahren werden kann. Bei diesem Rad kommt eine Gangschaltung mit sechzehn, achtzehn, einundzwanzig oder mehr Gängen zum Einsatz. Die meisten Cyclocrossräder haben eine Scheibebremse. Das Gravelbike und das Mountainbike sind Alternativen',
  'image_source': 'Fahrrad.de',
  'image_url': 'https://images.internetstores.de/products//782356/02/ccfdde/Cannondale_CAADX_Tiagra_FRD[640x480].jpg?forceSize=true&forceAspectRatio=true&useTrim=true',
  'name': 'Cyclocross / Crosser',
  'id': 'cyclocross'
}, {
  'description': 'Ein Laufrad für Kinder kommt ohne Kurbel, Pedalen und Gänge aus. Das Kind kommt mit Hilfe der Füße voran, die sich am Boden abstossen',
  'image_source': 'Fahrrad.de',
  'image_url': 'https://images.internetstores.de/products//704726/02/d1047c/s_cool_pedeX_race_Lemon_Green_Matt[640x480].jpg?forceSize=true&forceAspectRatio=true&useTrim=true',
  'name': 'Kinderlaufrad',
  'id': 'kinderlaufrad'
}, {
  'description': 'Ein Fatbike ist ein Mountainbike mit besonders breiten (dicken) Reifen. Ein Mountainbike (oder MTB) ist ein Fahrrad, welches sich für den Einsatz auf nicht Asphalt, asphaltierte Wegeierten, unwegsamen Gelände (unbefestigte Wege / Feldwege / Forstwege / Schotter / Wald / Offroad) eignet. Besonders beliebt ist die Fahrt auf dünnen Pfaden, die durch den Wald oder unebenes Gelände (Singletrack) oder durch sogenannte Bikeparks führen. Auch Abfahrten von Bergen oder Hügeln (Downhill) sind Einsatzgebiete von Mountainbikes. Natürlich eignet sich das Rad auch für eine Fahrt ins Büro oder für eine Ausfahrt. Allerdings ist zu beachten, daß die Fahrräder schwer sind und mit ihren breiten (dicken) Reifen für viel Reibung sorgen. Mountainbikes haben einen geraden Lenker und verfügen über eine Gangschaltung mit sechzehn, achtzehn, einundzwanzig, oder mehr Gängen. Des Weiteren verfügen viele Mountainbikes über eine Scheibenbremse',
  'image_source': 'Singletrack Magazine',
  'image_url': 'http://singletrackworld.com/wp-content/blogs.dir/1/files/2016/03/DSC04498.jpg',
  'name': 'Fatbike',
  'id': 'fatbike'
}, {
  'description': 'Ein Fitnessbike ist ein Rad, welches zum Sport treiben genutzt werden kann (Sportrad / Sportfahrrad). Es ist ähnlich einem Rennrad, nur dass es einen geraden Lenker hat und Geschwindigkeit eine nicht so große Rolle spielt. Meist kommt ein leichter Aluminiumrahmen zum Einsatz. Die dünnen Reifen sorgen für geringen Reibungsverlust auf der Strasse oder dem Radweg. Es verfügt über eine Gangschaltung mit sechzehn, achtzehn, einundzwanzig, oder mehr Gängen. Das Fahrrad kann natürlich auch für Fahrten in der Stadt und für Ausflüge am Wochenende genutzt werden. Eine Alternative ist das Rennrad',
  'image_source': 'Canyon Bikes',
  'image_url': 'https://static.canyon.com/_img/navi-bikes/fitness-roadlite.jpg',
  'name': 'Fitnessbike',
  'id': 'fitnessbike'
}, {
  'description': 'Ein Gravelfahrrad oder Gravelbike ist ein Fahrrad mit dem man auch abseits der befestigten Wege (unbefestigte Wege / Feldwege / Schotter / Schotter / Wald / Offroad) unterwegs sein kann. Während es viele Ähnlichkeiten mit einem Cyclocross hat, ist es jedoch für längere Touren konzipiert wurden - eine Art Trekkingfahrrad für unwegsames Gelände. Das Rad wird meist ein Aluminiumrahmen verwendet. Der Lenker entspricht dem des Rennrads (Rennradlenker). Die Reifen sind dicker als beim Rennrad und verfügen über ausreichend Profil, so daß auch unebenes Gelände befahren werden kann. Die Montage von Schutzblechen und Gepäckträger sind üblich. Eine Alternative ist das Cyclocross / Gravelbike oder ein Trekkingrad / Trekkingbike',
  'image_source': 'Canyon Bikes',
  'image_url': 'https://static.canyon.com/_img/navi-bikes/road-grail.jpg',
  'name': 'Gravelbike',
  'id': 'gravelbike'
}, {
  'description': 'Ein Klappfahrrad oder Faltrad zeichnet sich dadurch aus, daß es zusammengeklappt werden kann. Somit kann das Fahrrad zum Beispiel mit ins Büro oder die Bahn genommen werden. Es ist daher sehr klein und kompakt und findet daher auch bequem Platz im Kofferraum eines PKWs. Auch die Laufräder sind viel kleiner als bei anderen Fahrrädern (20 bis 24 Zoll). Sie haben einen geraden Lenker und man sitzt meist aufrecht. Bei diesem Rad kommen Gangschaltungen mit sieben, neun oder mehr Gängen zum Einsatz. Natürlich gibt es auch ein E-Klappfahrrad oder E-Faltrad',
  'image_source': 'Fahrrad.de',
  'image_url': 'https://www.fahrrad.de/sf-media/iscommerce-shop/category_brand_header/FDE/listHeader/categoryHeader_36907_1.jpeg',
  'name': 'Klappfahrrad / Faltrad',
  'id': 'klappfahrrad'
}, {
  'description': 'Ein E-Klappfahrrad oder E-Faltrad st ein Fahrrad mit Elektroantrieb (E-Bike / Pedelec / E-Fahrrad / E-Rad). Das E-Bike zeichnet sich dadurch aus, daß es zusammengeklappt werden kann. Somit kann das Fahrrad zum Beispiel mit ins Büro oder die Bahn genommen werden. Es ist daher sehr klein und kompakt und findet daher auch bequem Platz im Kofferraum eines PKWs. Auch die Laufräder sind viel kleiner als bei anderen Fahrrädern (20 bis 24 Zoll). Sie haben einen geraden Lenker und man sitzt meist aufrecht. Bei diesem Rad kommen Gangschaltungen mit sieben, neun oder mehr Gängen zum Einsatz',
  'image_source': 'Zweirad Stadler',
  'image_url': 'https://shop.zweirad-stadler.de/out/pictures/generated/product/1/540_400_90/hercules-rob-fold-f7-nf-schwarz-89771.jpg',
  'name': 'E-Klappfahrrad / E-Faltrad',
  'id': 'e-klappfahrrad'
}, {
  'description': 'Ein BMX ist ein Fahrrad mit dem man Stunts machen kann. Die Räder sind sehr stabil und wendig. Der Rahmen ist meist aus Aluminium (Aluminiumrahmen). Die Laufräder haben meist eine Größe von nur 20 Zoll. Der Lenker lässt sich um 360 Grad drehen, womit Stunts wie der Barspin durchgeführt werden können. Das Hinterrad verfügt meist über eine Achsverlängerung (Peg), auf dem man stehen kann',
  'image_source': 'Zweirad Stadler',
  'image_url': 'https://shop.zweirad-stadler.de/out/pictures/generated/product/1/540_400_90/sibmx20fs18400sw-blau2018(2).jpg',
  'name': 'BMX',
  'id': 'bmx'
}, {
  'description': 'Ein Tandem ist ein Fahrrad für zwei Personen. Die vordere Person lenkt das Rad, die hintere Person tritt nur in die Pedalen. Mittlerweile gibt es neben den konventionellen Tandemfahrrad und andere Ausführungen, wie z.B. Stufentandem, Liegeradtandem, faltbare Tandems und Mountaintandems. Des Weiteren gibt es Tandemfahrräder für drei Personen, welches auch Tridem, Triplett oder Dreischnell genannt wird. Ebenso es ein E-Tandem',
  'image_source': 'Poison Bikes',
  'image_url': 'https://www.poison-bikes.de/images/Raeder/2018/Dioxin/Dioxin-Shimano-105-Rennrad.jpg',
  'name': 'Tandem',
  'id': 'tandem'
}, {
  'description': 'Ein E-Tandem ist ein Fahrrad mit Elektroantrieb (E-Bike / Pedelec / E-Fahrrad / E-Rad). Ein E-Tandem ist Fahrrad für zwei Personen. Die vordere Person lenkt das Rad, die hintere Person tritt nur in die Pedalen. Mittlerweile gibt es neben den konventionellen Tandemfahrrad und andere Ausführungen, wie z.B. Stufentandem, Liegeradtandem, faltbare Tandems und Mountaintandems. Des Weiteren gibt es Tandemfahrräder für drei Personen, welches auch Tridem, Triplett oder Dreischnell genannt wird. Ebenso gibt es nicht elektrische Tandem',
  'image_source': 'Poison Bikes',
  'image_url': 'https://www.poison-leihrad.de/files/Poison-Leihrad/Bilder/dioxin-shimano-alfine-e-bike-gruen.jpg',
  'name': 'E-Tandem',
  'id': 'e-tandem'
}, {
  'description': 'Ein Liegerad ist ein Fahrrad auf dem man liegt bzw. eine liegende Sitzposition einnimmt. Es kommt hier ein Netzsitz oder Schalensitz zum Einsatz. Das Trettlager und die Pedalen sind vorn angebracht, wo die Kraft direkt zum Antrieb der Vorderräder genutzt wird. Meist ist ein Liegerad mit zwei Rädern (eins vorn, eins hinten) von 20 Zoll ausgestattet, ein Liegedreirad mit drei. Oftmals werden bei Liegerädern auch Gangschaltungen verbaut',
  'image_source': 'Liegerad-Shop',
  'image_url': 'https://www.liegerad-shop.de/upload/16.jpg',
  'name': 'Liegerad',
  'id': 'liegerad'
}, {
  'description': 'Ein Conferencebike bietet Platz für bis zu sieben Personen. Jede Person hat einen eigenen Sitz und kann selbst in die Pedale treten. Allerdings kann nur eine Person das Fahrrad lenken. Ein Conferencebike ist schwer und wiegt ca. 200 kg. Es ist breit und lang, und daher unhandlich',
  'image_source': 'Wikipedia',
  'image_url': 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/ConferenceBike_%28fcm%29.jpg/1200px-ConferenceBike_%28fcm%29.jpg',
  'name': 'Conferencebike',
  'id': 'conferencebike'
}, {
  'description': 'Ein Allrounder oder Allroundbike ist ein Fahrrad für faste alle Zwecke. Es kann daher fast überall gefahren werden und ist eine guter Kompromiss zwischen Onroad und Offroad. Es ist kommt meist ein Aluminiumrahmen zum Einsatz. Die alltagstauglichen Räder gibt es meist auch als Damenfahrrad. Meist kommen 28 Zoll Laufräder und profilierte Reifen zum Einsatz. Die Montage von Schutzblechen und Gepäckträger sind auch möglich. Manchmal wird auch ein Federgabel verbaut, um auch komfortabel über Waldwege und Forstwege zu kommen. Alternative sind das Trekkingrad / Trekkingbike, Mountainbike / MTB oder vielleicht auch ein Gravelbike',
  'image_source': 'Radfieber.de',
  'image_url': 'https://www.radfieber.de/out/pictures/master/product/1/6x_tour_gent_19_55_foggy_grey_my19.jpg',
  'name': 'Allroundbike / Allrounder',
  'id': 'allrounder'
}, {
  'description': 'Ein Lastenrad oder Transportrad ist ein Fahrrad mit dem kleine, mittelgroße und große Lasten (bis 300 kg) transportiert werden können. Es gibt verschiedene Ausführungen, wie z.B. den Frontlader oder Vorderlader, bei dem die Last vor dem Lenker zu finden ist. So werden sie auch schon zu Verkaufsläden, wie beim Kaffeefahrrad (Coffeebike) oder dem Eisfahrrad. Auch die deutsche Post nutzt Lastenfahrräder, um Briefe auszuliefern. Auch Logistikunternehmen, wie UPS, setzen auf Lastenräder. Es können auch Personen transportiert werden, wie bei einer Rikscha. Lastenräder müssen stabil sein, und schwere Lasten tragen können. Sie sind daher schwer und unhandlich. Es kommen meist zwei bis drei Laufräder zum Einsatz (20 Zoll). Natürlich gibt es auch ein E-Lastenrad, welches einen Elektroantrieb / eine Batterie hat. Eine Alternative kann die Rikscha sein, wenn es darum geht Menschen zu transportieren. Des Weiteren gibt ein E-Lastenrad oder E-Transportrad',
  'image_source': 'Detektor.fm',
  'image_url': 'https://detektor.fm/wp-content/uploads/2018/08/lastenrad-transporter_thibaud-schremser-e1535632406736-579x434.jpg',
  'name': 'Lastenrad',
  'id': 'lastenrad'
}, {
  'description': 'Ein E-Lastenrad oder E-Transportrad ist ein Fahrrad mit Elektroantrieb (E-Bike / Pedelec / E-Fahrrad / E-Rad). Mit dem Lastenrad können kleine, mittelgroße und große Lasten (bis 300 kg) transportiert werden können. Es gibt verschiedene Ausführungen, wie z.B. den Frontlader oder Vorderlader, bei dem die Last vor dem Lenker zu finden ist. So werden sie auch schon zu Verkaufsläden, wie beim Kaffeefahrrad (Coffeebike) oder dem Eisfahrrad. Auch die deutsche Post nutzt Lastenfahrräder, um Briefe auszuliefern. Auch Logistikunternehmen, wie UPS, setzen auf Lastenräder. Es können aber auch Personen transportiert werden, wie bei einer Rikscha. Lastenräder müssen stabil sein, und schwere Lasten tragen können. Sie sind daher schwer und unhandlich. Es kommen meist zwei bis drei Laufräder zum Einsatz (20 Zoll). Natürlich gibt es auch ein E-Lastenrad, welches einen Elektroantrieb / eine Batterie hat. Eine Alternative kann die Rikscha sein, wenn es darum geht Menschen zu transportieren',
  'image_source': 'Mybike Magazin',
  'image_url': 'https://www.mybike-magazin.de/typo3temp/pics/9_938ecd4d67.jpeg',
  'name': 'E-Lastenrad',
  'id': 'e-lastenrad'
}, {
  'description': 'Mit einer Rikscha, auch Fahrradrikscha oder Fahrradtaxi genannt, werden Personen transportiert. Es war in den heute entwickelten Ländern vor der Industrialisierung ein Transportmittel, dient heute oft nur noch dazu, die eigenen Kinder zu transportieren oder Touristen zu kutschieren. In manchen Entwicklungsländern wird es heutzutage immer noch verwendet. Es gibt zwei Varianten einer Rikscha, als Frontlader oder als Hecklader. Beide Versionen nutzen jedoch drei Laufräder. Die Fahrräder sind sehr schwer und unhandlich. Natürlich gibt es auch eine E-Rikscha. Eine Alternative kann ein entsprechendes Lastenrad sein',
  'image_source': 'Berlin Rikscha Tours',
  'image_url': 'http://www.berlin-rikscha-tours.de/wp-content/uploads/2015/06/scale-925-616-02.782281193.2-720x380.jpg',
  'name': 'Rikscha',
  'id': 'rikscha'
}, {
  'description': 'Eine E-Rikscha ist ein Fahrrad mit Elektroantrieb (E-Bike / Pedelec / E-Fahrrad / E-Rad). Mit einer Rikscha, auch Fahrradrikscha oder Fahrradtaxi genannt, werden Personen transportiert. Es war in den heute entwickelten Ländern vor der Industrialisierung ein Transportmittel, dient heute oft nur noch dazu, die eigenen Kinder zu transportieren oder Touristen zu kutschieren. In manchen Entwicklungsländern wird es heutzutage immer noch verwendet. Es gibt zwei Varianten einer Rikscha, als Frontlader oder als Hecklader. Beide Versionen nutzen jedoch drei Laufräder. Die Fahrräder sind sehr schwer und unhandlich. Natürlich gibt es auch eine nicht elektrische Rikscha',
  'image_source': 'Biketronic',
  'image_url': 'http://www.biketronic.at/images/biketronic/bikes/rikscha/rikscha-bilder/rikscha-web-1.jpg',
  'name': 'E-Rikscha',
  'id': 'e-rikscha'
}, {
  'description': 'Ein Rad für Kinder.',
  'image_source': 'Fahrrad.de',
  'image_url': 'https://images.internetstores.de/products//704942/02/8305de/s_cool_XXlite_18_steel_Green_Yellow_[640x480].jpg?forceSize=true&forceAspectRatio=true&useTrim=true',
  'name': 'Kinderfahrrad / Kinderrad',
  'id': 'kinderrad'
}, {
  'description': 'Ein Beachcruiser ist ein Fahrrad mit dem man cruisen kann. Es geht darum einfach nur langsam vorwärts zu rollen.',
  'image_source': 'Radfieber.de',
  'image_url': 'https://www.radfieber.de/out/pictures/generated/product/1/400_400_95/7939_0.jpg',
  'name': 'Beachcruiser',
  'id': 'beachcruiser'
}, {
  'description': 'Ein E-Rennrad ist ein Rennrad mit Elektroantrieb (E-Bike / Pedelec / E-Fahrrad / E-Rad). Das E-Bike dient vor allem der sportlichen Betätigung (Sportrad / Sportfahrrad). Spitzensportler sowie Amateure trainieren auf solchen einem solchem Rad ihre Ausdauer, in flachen Gebieten als auch in den Bergen. Rennradfahren ist ein beliebter Sport bei Jung und Alt. Die Sitzposition ist ebenso sportlich nach vorn neigend. Ein leichter Rahmen und die dünnen Reifen (18mm - 25mm) sorgen für eine schnelle, reibungsarme Fortbewegung mit diesem Fahrrad. Während ältere Modelle Rahmen aus Eisen (Eisenrahmen) bestückt sein können, haben moderne Rennräder meist Rahmen aus Carbon (Carbonrahmen) oder Aluminium (Aluminiumrahmen), womit sie sehr leicht sind. Spezifisch für Rennräder sind der Rennlenker / Rennradlenker, der ein Umfassen erlaubt. Bei Rennrädern kommt eine Gangschaltung mit sechzehn, achtzehn, einundzwanzig oder mehr Gängen zum Einsatz. Typische Hersteller sind Shimano (mit Dura-Ace, Ultegra, 105, Tiagra), SRAM oder Campagnolo. Natürlich gibt es auch ein nicht elektrisches Rennrad. Das Triathlonrad ist verwandt, das Fitnessbike eine Alternative',
  'image_source': 'Tour Magazin',
  'image_url': 'https://www.tour-magazin.de/typo3temp/pics/e_df8455fb91.jpeg',
  'name': 'E-Rennrad',
  'id': 'e-rennrad'
}, {
  'description': 'Ein Crossrad ist ein Fahrrad, welches auch abseits von Asphalt, asphaltierte Wegen gefahren werden kann (unbefestigte Wege / Feldwege / Forstwege / Schotter / Wald / Offroad). Es ist ein Mischung aus Fitnessbike und Mountainbike und wird vor allem zur sportlichen Betätigung eingesetzt. Besonders beliebt ist die Fahrt auf dünnen Pfaden, die durch den Wald oder unebenes Gelände (Singletrack). Der Rahmen ist meist aus Aluminium (Aluminiumrahmen). Der Lenker ist gerade. Die Reifen sind dicker als beim Fitnessrad und verfügen über ausreichend Profil, so daß auch unebenes Geländer befahren werden kann. Bei diesem Rad kommt eine Gangschaltung mit sechzehn, achtzehn, einundzwanzig oder mehr Gängen zum Einsatz. Die meisten Crossräder haben eine Scheibebremse. Das Gravelbike, das Cyclocross und das Mountainbike sind Alternativen',
  'image_source': 'Fahrrad.de',
  'image_url': 'https://images.internetstores.de/products//857929/02/ef4d9d/Cube_Cross_Allroad_Iridium_n_Green[640x480].jpg?forceSize=true&forceAspectRatio=true&useTrim=true',
  'name': 'Crossrad',
  'id': 'crossrad'
}, {
  'description': 'Ein E-Crossrad ist ein Crossrad mit Elektroantrieb (E-Bike / Pedelec / E-Fahrrad / E-Rad). Das E-Bike kann auch abseits von Asphalt, asphaltierte Wegen gefahren werden (unbefestigte Wege / Feldwege / Forstwege / Schotter / Wald / Offroad). Es ist ein Mischung aus Fitnessbike und Mountainbike und wird vor allem zur sportlichen Betätigung eingesetzt. Besonders beliebt ist die Fahrt auf dünnen Pfaden, die durch den Wald oder unebenes Gelände (Singletrack). Der Rahmen ist meist aus Aluminium (Aluminiumrahmen). Der Lenker ist gerade. Die Reifen sind dicker als beim Fitnessrad und verfügen über ausreichend Profil, so daß auch unebenes Geländer befahren werden kann. Bei diesem Rad kommt eine Gangschaltung mit sechzehn, achtzehn, einundzwanzig oder mehr Gängen zum Einsatz. Die meisten Crossräder haben eine Scheibebremse. Das Gravelbike, das Cyclocross und das Mountainbike sind Alternativen',
  'image_source': 'Fahrrad.de',
  'image_url': 'https://images.internetstores.de/products//857131/02/3be215/Cube_Cross_Hybrid_ONE_500_Allroad_Iridium_n_Blue[640x480].jpg?forceSize=true&forceAspectRatio=true&useTrim=true',
  'name': 'E-Crossrad',
  'id': 'e-crossrad'
}];
