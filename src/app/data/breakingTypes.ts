export const BreakingTypes = [
  {
    'description': 'Bei einer Felgenbremse erzeugen die Bremsbacken, die bei Betätigung der Bremse, auf die Felge der Laufräder aufsetzen, eine Bremswirkung.',
    'image_source': 'MTB Fit',
    'image_url': 'https://mtb-fit.at/wp-content/uploads/2015/04/Felgenbremse-Rennrad_5788.jpg',
    'name': 'Felgenbremse',
    'advantages': ['billiger als Scheibenbremsen', 'leichte Wartung', 'geringes Gewicht'],
    'disadvantages': ['verschleißen schneller als Scheibenbremsen', 'schlechte Bremswirkung bei Nässe', 'nicht hitzebeständig'],
  }, {
    'description': 'Bei einer Scheibenbremse kommt eine Scheibe (Disc) zum Einsatz. Bei der Betätigung der Bremse werden die Bremsbeläge gegen die Bremsscheibe gedrückt. Durch die entstehende Reibung entsteht eine Bremswirkung.',
    'image_source': 'MTB Fit',
    'image_url': 'https://mtb-fit.at/wp-content/uploads/2015/04/Shimano_Disc_Brake_Scheibenbremse.jpg',
    'name': 'Scheibenbremse',
    'advantages': ['zuverlässige Bremswirkung bei fast jeder Witterungsbedingung', 'wartungsarm', 'hitzebeständig'],
    'disadvantages': ['teurer als Felgenbremsen', 'die Wartung ist umfangreicher als bei Felgenbremsen'],
  },
];
