export const FolkTypes = [
  {
    'description': 'Eine Starrgabel ist starr, d.h. sie bietet keinerlei Federung. Typische Materialien sind Carbon, Aluminium oder auch Stahl.',
    'image_source': 'Poison Bikes',
    'image_url': 'https://www.poison-bikes.de/images/Parts_Zubehoer_ab2010/Gabeln/Starrgabeln/2018/Fixie_Front_kp.jpg',
    'name': 'Starrgabel',
    'advantages': ['billiger als Federgabeln', 'wenig Verschleiß', 'wartungsarm'],
    'disadvantages': ['keine Federung'],
  }, {
    'description': 'Eine Federgabel bietet eine Federung und ist damit praktisch für Fahrten auf unwegsamen Gelände (Offroad).',
    'image_source': 'Rose',
    'image_url': 'https://www.rosebikes.de/images/0tSdo7uSmTNQKuFoDmf_ZnihGBcgYwc1DCtG0cHuY0k/resize:fit:512:512:1/gravity:no/background:FFFFFF/bG9jYWw6Ly8vcHJvZHVjdC8yMjYxODk0XzEucG5n.webp?39972972',
    'name': 'Federgabel',
    'advantages': ['gute Federung auf unwegsamen Gelände'],
    'disadvantages': ['meist teurer als Starrgabeln', 'regelmässige Wartung erforderlich', 'Verschleiß'],
  },
];
