export const FrameTypes = [{
  'description': 'Viele Jahre lang war Stahl eines der wichtigsten Materialien in der Fahrradherstellung. Während das Material gute Dämpfungseigenschaften besitzt, rostet es doch irgendwann. Ausserdem ist Stahl schwerer als andere Materialien.',
  'image_source': 'Poison Bikes',
  'image_url': 'https://www.poison-bikes.de/images/Kategoriebilder/Cyanit_rahmen.jpg',
  'name': 'Stahl',
  'id': 'stahl',
}, {
  'description': 'Aluminium ist eines der am häufigsten verwendeten Materialien in der Fahrradherstellung. Das Material rostet nicht, und Aluminiumrahmen sind meist leichter als Stahlrahmen.',
  'image_source': 'Poison Bikes',
  'image_url': 'https://www.poison-bikes.de/images/Kategoriebilder/Taxin_rahmen.jpg',
  'name': 'Aluminium',
  'id': 'alu',
}, {
  'description': 'Carbon ist leichter als Aluminium, und daher besonders bei Rennrädern ein gefragtes Material. Allerdings sind Carbonrahmen meist teurer als Aluminium oder Stahl, da die Fertigung aufwendiger ist. Ein Riss im Carbonrahmen kann allerdings bedeuten, daß der Rahmen komplett ersetzt werden muss.',
  'image_source': 'Poison Bikes',
  'image_url': 'https://www.poison-bikes.de/images/Kategoriebilder/Oxygen_rahmen_rot.jpg',
  'name': 'Carbon',
  'id': 'carbon',
}, {
  'description': 'Titan ist ein sehr hochwertiges und teures Material. Es ist leichter und elastischer als andere Materialien.',
  'image_source': 'Poison Bikes',
  'image_url': 'https://www.poison-bikes.de/images/Kategoriebilder/Rahmen/Phosphor_Rahmen.jpg',
  'name': 'Titan',
  'id': 'titan',
}];
