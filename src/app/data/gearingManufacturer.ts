export const GearingManufacturer = [
  {
    'description': 'Shimano aus Japan ist einer der größten Hersteller von Schaltgruppen',
    'image_source': 'Wikimedia',
    'image_url': 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/Shimano.svg/500px-Shimano.svg.png',
    'gruppen': [
      {
        'id': 'Rennrad',
        'groups': ['Dura-Ace Di2', 'Dura-Ace', 'Ultegra Di2', 'Ultegra', '105', 'Sora', 'Claris', '2200/2300', 'Tourney'],
      },
      {
        'id': 'Mtb',
        'groups': ['Saint', 'XTR Di2', 'XTR', 'XT Di2', 'XT Deore', 'SLX', 'ZEE', 'Deore', 'Alivio', 'Tourney', 'Altus', 'Acera']
      },
      {
        'id': 'Urban (Nabenschaltung)',
        'groups': ['Alfine', 'Nexus']
      }
    ],
    'name': 'Shimano',
    'id': 'shimano',
  },
  {
    'description': 'SRAM aus den USA wurde einst bekannt durch seine Grip-Shift Schaltung, bietet jetzt aber Schaltgruppen für alle möglichen Fahrradtypen an. Die Produktion von Nabenschaltungen wurden vor kurzem eingestellt.',
    'image_source': 'Wikimedia',
    'image_url': 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/SRAM_logo.svg/500px-SRAM_logo.svg.png',
    'gruppen': [
      {
        'id': 'Rennrad',
        'groups': ['Red eTap', 'Red', 'Force', 'Rival', 'Apex'],
      },
      {
        'id': 'Mtb',
        'groups': ['XX1 Eagle', 'X01 Eagle', 'XX1', 'X01', 'X1', 'GX', 'X9', 'X7', 'NX', 'X5']
      }
    ],
    'name': 'SRAM',
    'id': 'sram',
  },
  {
    'description': 'Campagnolo ist ein italienischer Hersteller von Schaltgruppen, der sich vor allem auf Rennräder konzentriert.',
    'image_source': 'Wikimedia',
    'image_url': 'https://upload.wikimedia.org/wikipedia/de/thumb/f/f8/Campagnolo_logo.svg/500px-Campagnolo_logo.svg.png',
    'gruppen': [
      {
        'id': 'Rennrad',
        'groups': ['Super Record', 'Record', 'Chorus', 'Potenza', 'Centaur (bis 2002: Daytona, bis 2000: Athena)'],
      }
    ],
    'name': 'Campagnolo',
    'id': 'campagnolo',
  },
  {
    'description': 'Fallbrook Technologies ist bekannt für seine NuVinci Nabenschaltungen.',
    'image_source': 'Fallbrook Technologies',
    'image_url': 'https://www.fallbrooktech.com/sites/all/themes/fallbrook/images/png/fallbrook-logo-bottom.png',
    'gruppen': [
      {
        'id': 'Urban (Nabenschaltung)',
        'groups': ['NuVinci N380', 'NuVinci N350', 'NuVinci N330'],
      }
    ],
    'name': 'Fallbrook',
    'id': 'fallbrook',
  },
  {
    'description': 'Rohloff ist ein deutscher Hersteller für Nabenschaltungen.',
    'image_source': 'Rohloff',
    'image_url': 'https://www.rohloff.de/typo3conf/ext/theme_mg_rohloff/Resources/Public/Images/logo-rohloff2.png',
    'gruppen': [
      {
        'id': 'Urban (Nabenschaltung)',
        'groups': ['Speedhub'],
      }
    ],
    'name': 'Rohloff',
    'id': 'rohloff',
  }
];

