export const GearingTypes = [
  {
    'description': 'Bei einer Kettenschaltung sind die Ritzel (Zahnkränze) extern, d.h. am äußeren Teil des Fahrrads angebracht. Die Schaltung ist daher der Witterung ausgesetzt und muss öfter gewartet werden als eine Nabenschaltung. Dafür hat sie einen höheren Übersetzungsbereich und besitzt oftmals mehr Gänge.',
    'image_source': 'Wikimedia',
    'image_url': 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Shimano_xt_rear_derailleur.jpg/440px-Shimano_xt_rear_derailleur.jpg',
    'name': 'Kettenschaltung'
  }, {
    'description': 'Bei einer Nabenschaltung ist am äußeren Teil des Fahrrads nur ein Ritzel angebracht. In der Nabe befindet sich eine Art Getriebe. Die Nabenschaltung hat einen geringeren Übersetzungsbereich und besitzt oft weniger Gänge als eine Kettenschaltung. Dafür ist sie wartungsärmer.',
    'image_source': 'Wikimedia',
    'image_url': 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/SRAM_iMotion_9_Coaster_brake.jpg/440px-SRAM_iMotion_9_Coaster_brake.jpg',
    'name': 'Nabenschaltung'
  },
];
