export const Questions = [
  {
    'question': 'Was ist der Hauptzweck?',
    'options': [
      {
        'value': '',
        'display': 'Egal',
        'active': true
      },
      {
        'value': 'um Sport zu treiben',
        'display': 'Sport treiben',
        'active': false
      },
      {
        'value': 'um ins Büro zu fahren',
        'display': 'zur Arbeit / ins Büro fahren',
        'active': false
      },
      {
        'value': 'um Touren zu fahren',
        'display': 'Touren fahren',
        'active': false
      },
      {
        'value': 'um Einkaufen zu fahren',
        'display': 'Einkauf erledigen',
        'active': false
      }
    ]
  },
  {
    'question': 'Wollen Sie damit auf unwegsamen Gelände fahren?',
    'options': [
      {
        'value': '',
        'display': 'Nö',
        'active': true
      },
      {
        'value': 'um auf unwegsamen Gelände zu fahren',
        'display': 'Ja',
        'active': false
      },
    ]
  },
  {
    'question': 'Wollen Sie verrückte Sachen damit machen?',
    'options': [
      {
        'value': '',
        'display': 'Nö',
        'active': true
      },
      {
        'value': 'um Stunts zu machen',
        'display': 'Ja, Stunts',
        'active': false
      },
      {
        'value': 'um Sprünge / Jumps durchzuführen',
        'display': 'Ja, Sprünge / Jumps',
        'active': false
      }
    ]
  },
  {
    'question': 'Wollen Sie mit mehreren Personen auf dem Fahrrad sitzen?',
    'options': [
      {
        'value': '',
        'display': 'Allein',
        'active': true
      },
      {
        'value': 'um zu zweit zu fahren',
        'display': '2 Personen',
        'active': false
      },
      {
        'value': 'um zu dritt zu fahren',
        'display': '3 Personen',
        'active': false
      }
    ]
  },
  {
    'question': 'Wollen Sie etwas transportieren?',
    'options': [
      {
        'value': '',
        'display': 'Nur mich',
        'active': true
      },
      {
        'value': 'um Lasten zu transportieren',
        'display': 'Lasten transportieren',
        'active': false
      },
      {
        'value': 'um Personen zu transportieren',
        'display': 'Personen transportieren',
        'active': false
      }
    ]
  }

];
