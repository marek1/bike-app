import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { BikeTypesActionsTypes } from '../actions/bikeTypes.actions';
import { ActionWithPayload } from '../interfaces/actionWithPayload';
import { apiUrl } from '../app.module';

@Injectable()
export class BikeTypesEffects {

  @Effect()
  retrieve$: Observable<Action> = this.actions$.pipe(
    ofType(BikeTypesActionsTypes.Retrieve),
    mergeMap((action: ActionWithPayload) => {
      return this.http.get(apiUrl + '/bike_types')
        .pipe(
          map(data => ({ type: BikeTypesActionsTypes.RetrieveSuccess, payload: data })),
          catchError(() => of({ type: BikeTypesActionsTypes.RetrieveError }))
        );
    })
  );

  constructor(private http: HttpClient, private actions$: Actions) {}
}
