import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Action, select, Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { BikesActionsTypes } from '../actions/bikes.actions';
import { ActionWithPayload } from '../interfaces/actionWithPayload';
import { withLatestFrom } from 'rxjs/internal/operators';
import { getUserData } from '../reducer/user.reducer';
import { getUserId } from '../reducer';
import { apiUrl } from '../app.module';

@Injectable()
export class BikesEffects {

  @Effect()
  retrieve$: Observable<Action> = this.actions$.pipe(
    ofType(BikesActionsTypes.Retrieve),
    withLatestFrom(
      this.store$.pipe(select(getUserId)),
    ),
    mergeMap(([params, userData]: [ActionWithPayload, string]) => {
      console.log('params, userData : ', params, userData);
      const postObject = {
        // userDevice: (window as any).navigator.platform,
        // userAgent: (window as any).navigator.userAgent,
        // userLanguage: (window as any).navigator.language,
        // userLocation: '',
        user_id: userData,
        companyFilter: params.payload.companyFilter,
        requestString: params.payload.query
      };
      return this.http.post(apiUrl + '/ask', postObject)
        .pipe(
          map(data => ({ type: BikesActionsTypes.RetrieveSuccess, payload: data })),
          catchError(() => of({ type: BikesActionsTypes.RetrieveError }))
        );
    })
  );

  constructor(private store$: Store<any>,
              private http: HttpClient,
              private actions$: Actions) {}
}
