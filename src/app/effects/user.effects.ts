import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { ActionWithPayload } from '../interfaces/actionWithPayload';
import { UserActionsTypes } from '../actions/user.actions';
import { apiUrl } from '../app.module';

@Injectable()
export class UserEffects {

  @Effect()
  retrieve$: Observable<Action> = this.actions$.pipe(
    ofType(UserActionsTypes.Retrieve),
    mergeMap((action: ActionWithPayload) => {
      const postObject = {
        userIpAddress: (window as any)['ipAddress'] !== undefined ? (window as any)['ipAddress'] : '',
        userDevice: (window as any).navigator.platform,
        userAgent: (window as any).navigator.userAgent,
        userLanguage: (window as any).navigator.language,
        userLocation: ''
      };
      return this.http.post(apiUrl + '/get_user', postObject)
        .pipe(
          map(data => ({ type: UserActionsTypes.RetrieveSuccess, payload: data })),
          catchError(() => of({ type: UserActionsTypes.RetrieveError }))
        );
    })
  );

  constructor(private http: HttpClient, private actions$: Actions) {}
}
