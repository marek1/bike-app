export interface Bike {
  antrieb: string;
  bike_id: number;
  bike_type_id: number;
  break_type: string;
  fahrwerk: string;
  frame: string;
  framesize: string;
  gabel: string;
  gearing_type: string;
  img_url: string;
  manufacturer: string;
  name: string;
  name_bike_type: string;
  places_of_use: string;
  price_from_in_euro: string;
  purpose: string;
  schaltgruppe: string;
  tags: string;
  url: string;
  wheel_manufacturer: string;
  wheelsize_in_inches: string;
}
