import { BikeTypeLight } from './bikeTypeLight';

export interface BikeType extends BikeTypeLight {
  categories: string;
  description: string;
  on_negative_exclude: string;
  on_negative_include: string;
  places_of_use: string;
  purpose: string;
  seating_position: string;
  stearing_bar: string;
  type_name: string;
}
