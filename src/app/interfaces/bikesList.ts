import { BikeTypeLight } from './bikeTypeLight';
import { Bike } from './bike';

export interface BikesList {
  bike_types: BikeTypeLight[];
  bikes: Bike[];
}
