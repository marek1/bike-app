import { BikeTypesActionsTypes, BikeTypesActionsUnion } from '../actions/bikeTypes.actions';

export interface BikeTypesState {
  loading: boolean;
  error: boolean;
  data: any[];
}

export const initialState: BikeTypesState = {
  loading: false,
  error: false,
  data: []
};

export const reducer = (
  state = initialState,
  action: BikeTypesActionsUnion): BikeTypesState => {
  switch (action.type) {
    case BikeTypesActionsTypes.Retrieve:
      return {
        loading: true,
        error: false,
        data: []
      };
    case BikeTypesActionsTypes.RetrieveSuccess:
      return {
        ...state,
        loading: false,
        error: false,
        data: action.payload
      };
    case BikeTypesActionsTypes.RetrieveError:
      return {
        ...state,
        loading: false,
        error: true,
        data: []
      };
    default:
      return state;
  }
};

export const isLoading = (state: BikeTypesState) => state.loading;
export const getBikeTypesData = (state: BikeTypesState) => state.data;
