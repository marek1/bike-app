import { BikesActionsTypes, BikesActionsUnion } from '../actions/bikes.actions';
import { BikesList } from '../interfaces/bikesList';
import { CompanyFilterAndQuery } from '../interfaces/companyFilterAndQuery';
import { Bike } from '../interfaces/bike';

export interface BikesState {
  lastRequest: CompanyFilterAndQuery;
  loading: boolean;
  error: boolean;
  data: BikesList;
}

export const initialState: BikesState = {
  lastRequest: {
    companyFilter: '',
    query: ''
  },
  loading: false,
  error: false,
  data: {
    bike_types: [],
    bikes: []
  }
};

export const reducer = (
  state = initialState,
  action: BikesActionsUnion): BikesState => {
  switch (action.type) {
    case BikesActionsTypes.Retrieve:
      return {
        lastRequest: action.payload,
        loading: true,
        error: false,
        data: {
          bike_types: [],
          bikes: []
        }
      };
    case BikesActionsTypes.RetrieveSuccess:
      return {
        ...state,
        loading: false,
        error: false,
        data: action.payload
      };
    case BikesActionsTypes.RetrieveError:
      return {
        ...state,
        loading: false,
        error: true
      };
    default:
      return state;
  }
};

export const isLoading = (state: BikesState) => state.loading;
export const hasError = (state: BikesState) => state.error;
export const getLastRequest = (state: BikesState) => state.lastRequest;
export const getBikesData = (state: BikesState) => state.data;
