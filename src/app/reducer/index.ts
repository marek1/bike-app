import * as fromUser from './user.reducer';
import * as fromBikes from './bikes.reducer';
import * as fromBikeTypes from './bikeTypes.reducer';

import { ActionReducer, ActionReducerMap, combineReducers, createFeatureSelector, createSelector, MetaReducer } from '@ngrx/store';
import { InjectionToken } from '@angular/core';

export interface AppState {
  user: fromUser.UserState;
  bikes: fromBikes.BikesState;
  bikeTypes: fromBikeTypes.BikeTypesState;
}

export interface IState {
  app: AppState;
}

export const AppReducers = {
  user: fromUser.reducer,
  bikes: fromBikes.reducer,
  bikeTypes: fromBikeTypes.reducer
};

export const reducers = combineReducers(AppReducers);

export const reducerToken = new InjectionToken<ActionReducerMap<IState>>('Reducers');

export function getReducers() {
  return {
    app: reducers,
  };
}

export const reducerProvider = [
  { provide: reducerToken, useFactory: getReducers }
];

// SELECTORS :

export const getBikesModuleState = createFeatureSelector<AppState>('app');

// export const getBikeTypesModuleState = createFeatureSelector<State>('bikeTypes');

export const getBikesState = createSelector(
  getBikesModuleState,
  (state: AppState) => {
    return state.bikes;
  }
);

export const getUserState = createSelector(
  getBikesModuleState,
  (state: AppState) => {
    return state.user;
  }
);

export const getBikesData = createSelector(
  getBikesState,
  fromBikes.getBikesData,
);

export const isBikesLoading = createSelector(
  getBikesState,
  fromBikes.isLoading,
);

export const hasBikesError = createSelector(
  getBikesState,
  fromBikes.hasError,
);

export const getLastRequest = createSelector(
  getBikesState,
  fromBikes.getLastRequest
);

export const getUserId = createSelector(
  getUserState,
  fromUser.getUserId
);

export const getUserHistory = createSelector(
  getUserState,
  fromUser.getUserHistory
);
