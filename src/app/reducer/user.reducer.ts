import { UserActionsTypes, UserActionsUnion } from '../actions/user.actions';
import { Bike } from '../interfaces/bike';

export interface UserData {
  id: string;
  history: Bike[];
}

const initialUser = {
  id: '',
  history: []
};
export interface UserState {
  loading: boolean;
  error: boolean;
  data: UserData;
}

export const initialState: UserState = {
  loading: false,
  error: false,
  data: initialUser
};

export const reducer = (
  state = initialState,
  action: UserActionsUnion): UserState => {
  switch (action.type) {
    case UserActionsTypes.Retrieve:
      return {
        loading: true,
        error: false,
        data: initialUser
      };
    case UserActionsTypes.RetrieveSuccess:
      return {
        ...state,
        loading: false,
        error: false,
        data: action.payload
      };
    case UserActionsTypes.RetrieveError:
      return {
        loading: false,
        error: true,
        data: initialUser
      };
    default:
      return state;
  }
};

export const isLoading = (state: UserState) => state.loading;
export const getUserData = (state: UserState) => state.data;
export const getUserId = (state: UserState) => state.data.id;
export const getUserHistory = (state: UserState) => state.data.history;

