import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RetrieveUserAction } from '../actions/user.actions';
import { Store } from '@ngrx/store';
import { apiUrl } from '../app.module';

@Injectable()
export class ClickService {

  constructor(private store$: Store<any>,
              private http: HttpClient) {}

  postClickEvent(user_id: string, bike_type_id: number = 0, bike_id: number = 0) {
    const postObject = {
      // userDevice: (window as any).navigator.platform,
      // userAgent: (window as any).navigator.userAgent,
      // userLanguage: (window as any).navigator.language,
      // userLocation: '',
      user_id,
      bike_type_id,
      bike_id
    };
    this.http.post(apiUrl + '/clicked', postObject).subscribe((data) => {
      console.log('... ' + data);
      // after each re-submit also re-request the user (to get history)
      this.store$.dispatch(new RetrieveUserAction());
    });
  }

}
